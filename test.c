#include <stdio.h>
#include <stdlib.h>
int main(){
    void *p = malloc(sizeof(int));
    *((int*)p) = 2;

    printf("The var on the heap : %d", *((int*)p));

    return 0;
}
